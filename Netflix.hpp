// ---------
// Collatz.h
// ---------

#ifndef Netflix_h
#define Netflix_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <iterator> // istream_iterator

using namespace std;

/**
 * @param an istream
 * @param an ostream
 */
void netflix_solve (istream&, ostream&);

/**
 * @param an int
 */
double netflix_eval (int person);

#endif // Netflix_h