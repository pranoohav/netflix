This project essentially tackles a simplified version of the Netflix Challenge. The goal is to be able to predict user ratings for films based on past ratings without information about the user or film. It was completed in C++.

This is a link to read more about the challenge: https://www.netflixprize.com/