// ---------------
// TestNetflix.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <iterator> // istream_iterator
#include <sstream>  // istringtstream, ostringstream

#include "gtest/gtest.h"

#include "Netflix.hpp"

using namespace std;

// -----------
// TestNetflix
// -----------

// ----
// solve
// ----

TEST(NetflixFixture, solve1) {
    istringstream         iss("10003:\n1515111\n");
    ostringstream         oss;
    netflix_solve(iss, oss);
    ASSERT_EQ("10003:\n3\nRMSE: 0\n", oss.str());
}

TEST(NetflixFixture, solve2) {
    istringstream         iss("10011:\n1624701\n2646826\n");
    ostringstream         oss;
    netflix_solve(iss, oss);
    ASSERT_EQ("10011:\n4.1\n3.9\nRMSE: 0.08\n", oss.str());
}

TEST(NetflixFixture, solve3) {
    istringstream         iss("10017:\n2280428\n1129341\n1377724\n");
    ostringstream         oss;
    netflix_solve(iss, oss);
    ASSERT_EQ("10017:\n3.4\n3.6\n3.4\nRMSE: 0.69\n", oss.str());
}

TEST(NetflixFixture, solve4) {
    istringstream         iss("10034:\n2107088\n69088\n");
    ostringstream         oss;
    netflix_solve(iss, oss);
    ASSERT_EQ("10034:\n2.6\n3\nRMSE: 1.08\n", oss.str());
}

TEST(NetflixFixture, solve5) {
    istringstream         iss("10039:\n102104\n");
    ostringstream         oss;
    netflix_solve(iss, oss);
    ASSERT_EQ("10039:\n2.5\nRMSE: 1.14\n", oss.str());
}

TEST(NetflixFixture, solve6) {
    istringstream         iss("10045:\n2314434\n10040:\n2417853\n1207062\n2487973\n");
    ostringstream         oss;
    netflix_solve(iss, oss);
    ASSERT_EQ("10045:\n3.6\n10040:\n3.4\n3.2\n3.6\nRMSE: 1.03\n", oss.str());
}

// ----
// eval
// ----


TEST(NetflixFixture, eval1) {
    double b = netflix_eval(1515111);
    ASSERT_EQ(b, 3.3);
}

TEST(NetflixFixture, eval2) {
    double b = netflix_eval(1093333);
    ASSERT_EQ(b, 3.8);
}

TEST(NetflixFixture, eval3) {
    double b = netflix_eval(1982605);
    ASSERT_EQ(b, 3.5);
}

TEST(NetflixFixture, eval4) {
    double b = netflix_eval(1534853);
    ASSERT_EQ(b, 3.9);
}