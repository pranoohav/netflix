// -----------
// Netflix.c++
// -----------

// --------
// includes
// --------

#include <cassert>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <math.h>
#include <unordered_map>
#include "boost/archive/binary_iarchive.hpp"
#include "boost/archive/binary_oarchive.hpp"
#include "boost/serialization/unordered_map.hpp"

#include "Netflix.hpp"

using namespace std;
using namespace boost::archive;
using map_type = unordered_map<int, char>;
using map_value_type = map_type::value_type;
using map_map_type = unordered_map<short, map_type>;

map_map_type ansCache;
map_map_type::const_iterator search1;
map_type::const_iterator search2;

unordered_map<int, double> avgCustCache;
unordered_map<int, double>::const_iterator avgCustIterator;

unordered_map<short, double> avgMovieCache;
unordered_map<short, double>::const_iterator avgMovieIterator;


int movie = 0;
int numPeople = 0;
double totalDiff = 0;
double avgMovieRating = 0;

double truncateFunc(double num, int numDigits) {
    double factor = 1;
    double prev = trunc(num); //remove integer portion
    num -= prev;
    //calculate decimal
    for(int i = 0; i < numDigits; ++i) {
        num *= 10;
        factor *= 10;
    }
    num = trunc(num);
    num /= factor;
    num += prev; //add back integer portion
    return num;
}

double netflix_eval(int person) {
    double rating = 3.7;
    //Need to look up value of i in cache and assign output to that value
    //if it doesn't exist, we simply assign it a 3
    int answer = (int)search2->second;
    avgCustIterator = avgCustCache.find(person);
    if(avgCustIterator != avgCustCache.end()){
        rating = avgCustCache[person];
    }
    rating = (rating * .54) + (avgMovieRating * .54) - .2;
    rating = truncateFunc(rating, 1);
    double diff = rating - answer;
    if(rating > 5) {
        rating = 5;
    }
    totalDiff += diff * diff;
    return rating;
}

// -------------
// netflix_solve
// -------------

void netflix_solve(istream& sin, ostream& sout) {
    //answers cache
    ifstream ans("kev-liangg+cmontminy-ProbeAnswers.bin");
    binary_iarchive bias(ans);
    bias >> ansCache;
    // avg customer rating cache
    ifstream ifs("kev-liangg+cmontminy-AvgCustomerRatings.bin");
    binary_iarchive bia(ifs);
    bia >> avgCustCache;
    // avg movie rating cache
    ifstream stream("kev-liangg+cmontminy-AvgMovieRatings.bin");
    binary_iarchive bi(stream);
    bi >> avgMovieCache;
    string line;
    int person = 0;
    while (!sin.eof()) {
        getline(sin, line);

        if(line.find(":") != string::npos){
            movie = stoi(line);
            sout << movie << ":\n";
            avgMovieIterator = avgMovieCache.find(movie);
            if(avgMovieIterator != avgMovieCache.end()){
                avgMovieRating = avgMovieCache[movie];
            }
            search1 = ansCache.find(movie);	
        } else if(line != ""){
            person = stoi(line);
            numPeople++;
            search2 = search1->second.find(person);
            sout << netflix_eval(person) << endl;
        }
    }
    sout << "RMSE: " << truncateFunc(sqrt(totalDiff / numPeople), 2) << endl;
}