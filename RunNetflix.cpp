// --------------
// RunNetflix.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Netflix.hpp"

// ----
// main
// ----

int main () {
    using namespace std;
    netflix_solve(cin, cout);
    return 0;
}